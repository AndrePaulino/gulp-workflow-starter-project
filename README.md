Gulp-Pug-Sass-Browser-sync
==========================

A robust starter project setup for GulpJS, Pug, SASS, AutoPrefixer, SourceMaps, BrowserSync for an easier development and Uglify, CssNano, MinifyHtml, ImageMin for deployment.


For this starter project, you'll need in your machine.

1. [NodeJS](http://nodejs.org) - Use the proper installer.
2. [GulpJS](https://github.com/gulpjs/gulp) - `$ npm install -g gulp`. (unix users may need sudo)

## Local Installation

1. Clone this repo, or download it.
2. Inside the directory, run `npm install`. (again, sudo for unix users)

## Usage

**Development mode**

Using pug for modular html, all the code goes in `index.pug`.

This will give you file watching, CSS injecting, browser synchronisation, auto-rebuild, source maps for the styles.

```shell
$ gulp
```

## Deploy with Gulp

Build your deployment version, with optimized HTML, JS, CSS and images.

```shell
$ gulp build
```

Clean the distribution version.

```shell
$ gulp clean:dist
```

Since optimize images it's a heavy process, `gulp build` creates a cache for them. To clean this cache.

```shell
$ gulp clean
```
