/**
 * Run the app, gulp -> (dev-version) and gulp build -> (dist-version)
 */

// TODO: add functionality to deploy for gh-pages with [gulp-gh-pages](https://github.com/rowoot/gulp-gh-pages)

var gulp             = require('gulp'),
    pug              = require('gulp-pug'),
    sass             = require('gulp-sass'),
    prefix           = require('gulp-autoprefixer'),
    useref           = require('gulp-useref'),
    uglify           = require('gulp-uglify'),
    cssnano          = require('gulp-cssnano'),
    minifyhtml       = require('gulp-minify-html'),
    imagemin         = require('gulp-imagemin'),
    cache            = require('gulp-cache'),
    gulpIf           = require('gulp-if'),
    del              = require('del'),
    sourcemap        = require('gulp-sourcemaps'),
    changed          = require('gulp-changed'),
    runSequence      = require('run-sequence'),
    browserSync      = require('browser-sync').create(),
    browserSyncBuild = require('browser-sync').create();

var messages = {
    reload: "<span color='green'>Reloading</span>"
}

/**
 * BrowseSync dev and dist
 */

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: 'app'
    }
  })
})

gulp.task('browserSyncBuild', function() {
  browserSync.init({
    server: {
      baseDir: 'dist'
    }
  })
})

/**
 * Compile sass and pug.
 * Sourcemap
 * Autoprefixer
 */

gulp.task('sass', function() {

  return gulp.src('app/assets/css/main.scss')

  .pipe(changed('app/assets/css', {
    extension: '.'
  }))

  .pipe(sourcemap.init())
  .pipe(sass({
    includePaths: ['css'],
    onError: browserSync.notify
  }))

  .pipe(prefix(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {
    cascade: false
  }))

  .pipe(sourcemap.write('/maps'))
  .pipe(browserSync.reload({
    stream: true
  }))
  .pipe(gulp.dest('app/assets/css'))
})

gulp.task('pug', function() {

  return gulp.src('app/_pugfiles/index.pug')
  .pipe(pug({
    pretty: "\t"
  }))

  .pipe(gulp.dest('app'))
})

/**
 * Watch task
 */

gulp.task('watch', function() {

  gulp.watch('app/assets/css/**/*', ['sass'])
  gulp.watch('app/_pugfiles/**/*.pug', ['pug'])
  gulp.watch('app/*.html', browserSync.reload)
  gulp.watch('app/assets/js/**/*.js', browserSync.reload)
})

/**
 * Optimize JS, CSS, HTML and Images
 */

gulp.task('useref', function() {

  return gulp.src('app/*.html', {
    base: 'app'
  })

  .pipe(useref())
  .pipe(gulpIf('*.js', uglify()))
  .pipe(gulpIf('*.css', cssnano()))
  .pipe(minifyhtml())
  .pipe(gulp.dest('dist'))
})

gulp.task('images', function() {

  return gulp.src('app/assets/img/**/*.+(png|jpg|jpeg|gif|svg)')
  .pipe(cache(imagemin({
    interlaced: true
  })))

  .pipe(gulp.dest('dist/assets/img'))
})

/**
 * Copy fonts/
 */

gulp.task('fonts', function() {
  return gulp.src('app/assets/fonts/**')
  .pipe(gulp.dest('dist/assets/fonts'))
})

/**
 * Clean the dist version and cache
 */

gulp.task('clean:dist', function() {
  return del.sync(['dist/**', '!dist', '!dist/assets/img/**'])
})

gulp.task('clean', function(cb) {
  return cache.clearAll(cb);
})

/**
 * Run the app default(dev) and build(dist)
 */

gulp.task('default', function(callback) {
  runSequence(
    'pug',
    ['sass', 'browserSync', 'watch'],
    callback
  )
})

gulp.task('build', function(callback) {
  runSequence(
    'clean:dist',
    'sass',
    'useref',
    ['images', 'fonts'],
    'browserSyncBuild',
    callback
  )
})
